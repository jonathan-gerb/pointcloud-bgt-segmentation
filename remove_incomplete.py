# small script to remove incomplete samples
import glob
import os
import argparse
from collections import defaultdict

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="small script to remove incomplete samples")
    parser.add_argument('--basedir', default="/datadrive/AHN3_pointcloud/POC_2",
                        help='path to the pointcloud dataset to remove missing samples from')
    args = parser.parse_args()

    for split in ['train', 'test']:
        dir_parsed = os.path.join(args.basedir, "parsed", split)
        dir_features = os.path.join(args.basedir, "features", split)
        dir_spg = os.path.join(args.basedir, "superpoint_graphs", split)
        
        # get all filepaths
        files_parsed = glob.glob(dir_parsed + "/*.h5")
        files_features = glob.glob(dir_features + "/*.h5")
        files_spg = glob.glob(dir_spg + "/*.h5")
        
        # construct dict to save all checks in
        # data_dict = defaultdict(lambda : {"parsed": "", "features": "", "spg": ""})
        data_dict = defaultdict(lambda : {"features": "", "spg": ""})
        
        # for fpath in files_parsed:
            # data_dict[os.path.basename(fpath).split(".")[0]]['parsed'] = fpath
        
        for fpath in files_features:
            data_dict[os.path.basename(fpath).split(".")[0]]['features'] = fpath
            
        for fpath in files_spg:
            data_dict[os.path.basename(fpath).split(".")[0]]['spg'] = fpath
        
        removecounter = 0
        for key, values in data_dict.items():
            print("-" * 50)
            print(f"checking key: {key}")
            # parsed, features, spg = False, False, False
            features, spg = False, False
            
            # if values['parsed'] == "":
            #     parsed = True
            #     print(f"    no parsed file present")
            if values['features'] == "":
                features = True
                print(f"    no features file present")
            if values['spg'] == "":
                spg = True
                print(f"    no spg file present")
                
            # if any([parsed, features, spg]):
            if any([features, spg]):
                print(f"    removing files for key: {key}")
                for fpath in values.values():
                    if not fpath == "": 
                        print(f"        removing: {fpath}")
                        os.remove(fpath)
                        removecounter += 1
                        
        print(f"{len(data_dict.keys())} elements in {split} dataset, removed {removecounter} keys")
