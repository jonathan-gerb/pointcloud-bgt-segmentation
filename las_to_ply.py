import numpy as np

import logging
import requests
import os
import glob
from laspy.file import File
import multiprocessing
import numpy as np
import random
from plyfile import PlyData, PlyElement
import struct
from multiprocessing import Pool
import math
import argparse
import json
import pandas as pd
import sys
from tqdm import tqdm

def get_metadata():
    url = "http://geodata.nationaalgeoregister.nl/ahn3/wfs?request=getFeature&service=WFS&typeName=ahn3_bladindex&version=2.0.0&outputFormat=application/json"
    resp = requests.get(url=url)
    data = resp.json()
    return data

def las_to_ply(lasfile, metadata_dict, test_points=0):
    global ply_dict
    file_key = metadata_dict['properties']['bladnr']

    # contents of metadata_dict: 
    # [100000, 531250], [105000, 531250], [105000, 525000], [100000, 525000], [100000, 531250] = metadata_dict['geometry']['coordinates'][0][0])
    # in meters
    # [topleft, topright, botleft, botright] = metadata_dict['geometry']['coordinates'][0][0]
    print(metadata_dict['geometry']['coordinates'])
    
    # works for almost all cases,
    topleft = metadata_dict['geometry']['coordinates'][0][0][0]
    topright = metadata_dict['geometry']['coordinates'][0][0][1]
    botright = metadata_dict['geometry']['coordinates'][0][0][2]
    botleft = metadata_dict['geometry']['coordinates'][0][0][3]
    botleft = [int(coord/10)*10 for coord in botleft]
    topright = [int(coord/10)*10 for coord in topright]
    topleft = [int(coord/10)*10 for coord in topleft]
    botright = [int(coord/10)*10 for coord in botright]
    
    print(f"botleft: {botleft}, topright: {topright}, topleft:{topleft}, botright: {botright}")
    
    # make areas of 250m2
    keys = []
    for x in range(topleft[0], topright[0], 250):
        for y in range(botright[1], topright[1], 250):
            key = (x, y)
            keys.append(key)
                
    # ply_dict = dict.fromkeys(keys, pd.DataFrame(0, index=np.arange(100000),columns=['x', 'y', 'z', 'scanangle', 'intensity', 'flag', 'label']))
    # range of points per 250m square seems to be ~500k to 3m for AHN3
    ply_dict = dict.fromkeys(keys, {})
    
    for key in ply_dict.keys():
        ply_dict[key] = {"counter": 0, "points": np.zeros((500000, 7))}
    
    # X, Y, Z, intensity, flag_byte, raw_classification, scan_angle_rank, user_data, pt_src_id, gps_time
    pointcloud = File(lasfile, mode='r')
    
    # load in all data from the pointcloud
    x_arr = np.array(pointcloud.x)
    y_arr = np.array(pointcloud.y)
    z_arr = np.array(pointcloud.z)
    scanangle_arr = np.array(pointcloud.scan_angle_rank)
    intensity_arr = np.array(pointcloud.Intensity)
    flag_arr = np.array(pointcloud.flag_byte)
    label_arr = np.array(pointcloud.classification)
    
    print(f"number of points in pointcloud: {x_arr.shape[0]}")
    
    all_points = x_arr.shape[0]
    if test_points > 0:
        indices = np.random.randint(x_arr.shape[0], size=test_points)
    else:
        indices = range(x_arr.shape[0])
    
    for i in tqdm(indices, desc=f"Sectioning pointcloud for area {file_key} (topleft={topleft})"):
        x = x_arr[i]
        y = y_arr[i]
        z = z_arr[i]
        scangle = scanangle_arr[i]
        intensity = intensity_arr[i]
        flag = flag_arr[i]
        label = label_arr[i]
        
        # find the right key for this point
        grid_x = math.floor((x - botleft[0]) / 250) * 250 + botleft[0]
        grid_y = math.floor((y - botleft[1]) / 250) * 250 + botleft[1]
        ply_key = (grid_x, grid_y)
        
        # newx = x - grid_x
        # newy = y - grid_y
        newx = x
        newy = y
        
        try:
            ply_dict[ply_key]["points"][ply_dict[(grid_x, grid_y)]["counter"]] = np.array([newx, newy, z, scangle, intensity, flag, label])
        except IndexError:
            # resize the output array if there is no more space in the array
            old_size = ply_dict[ply_key]["points"].shape[0]
            print(f"resizing key {ply_key} to: {int(old_size * 1.5)}")
            new_points = np.zeros((int(old_size * 1.5), 7))
            new_points[:old_size] = ply_dict[ply_key]["points"]
            ply_dict[ply_key]["points"] = new_points
            ply_dict[ply_key]["points"][ply_dict[(grid_x, grid_y)]["counter"]] = np.array([newx, newy, z, scangle, intensity, flag, label])
        ply_dict[ply_key]["counter"] += 1

    keys_to_delete = []
    
    # remove all the excess empty space for each array in ply_dict based on the counter
    for ply_key in ply_dict.keys():
        ply_dict[ply_key]["points"] = ply_dict[ply_key]["points"][:ply_dict[ply_key]["counter"]]
        if ply_dict[ply_key]['points'].shape[0] == 0:
            keys_to_delete.append(ply_key)
            
    for key_to_delete in keys_to_delete:
        del ply_dict[key_to_delete]
        
    
    conversion_dict = {
        "1": 0,
        "2": 1,
        "6": 2,
        "9": 3,
        "26": 4,
    }

    # convert classes in ahn data
    logging.info(f"replacing classes in AHN pointcloud")
    for ply_key in ply_dict.keys():
        for class_key in conversion_dict.keys():
                ply_dict[ply_key]["points"][ply_dict[ply_key]["points"] == int(class_key)] = conversion_dict[class_key]
        
    arguments = [(ply_key, data_dict) for ply_key, data_dict in ply_dict.items()]
    
    with multiprocessing.Pool(processes=5) as pool:
        results = pool.map(write_key_to_file, arguments)
        
    # write the 250x250 segments to individual .ply files
#     for ply_key in ply_dict.keys():
#         outpath = f"/datadrive/AHN3_pointcloud/POC_2/data/train/{str(int(ply_key[0])) + '_' + str(int(ply_key[1]))}.ply"
#         print(f"writing key: {ply_key}")
#         prop = [('x', 'f4'), ('y', 'f4'), ('z', 'f4'), ('scangle', 'f4'), ('intensity', 'f4'), ('flag', 'f4'), ('label', 'i4')]
#         vertex_all = np.empty(len(ply_dict[ply_key]['points']), dtype=prop)
#         for i in range(0, 6):
#             vertex_all[prop[i][0]] = ply_dict[ply_key]['points'][:, i]

#         vertex_all[prop[6][0]] = ply_dict[ply_key]['points'][:,-1].astype(np.int32)
#         ply = PlyData([PlyElement.describe(vertex_all, 'vertex')], text=True)
#         ply.write(outpath)
        

def write_key_to_file(arglist):
    [ply_key, data_dict] = arglist
    
    outpath = f"/datadrive/AHN3_pointcloud/POC_2/data/train/{str(int(ply_key[0])) + '_' + str(int(ply_key[1]))}.ply"
    print(f"writing key: {ply_key}")
    prop = [('x', 'f4'), ('y', 'f4'), ('z', 'f4'), ('scangle', 'f4'), ('intensity', 'f4'), ('flag', 'f4'), ('label', 'i4')]
    vertex_all = np.empty(len(data_dict['points']), dtype=prop)
    for i in range(0, 6):
        vertex_all[prop[i][0]] = data_dict['points'][:, i]

    vertex_all[prop[6][0]] = data_dict['points'][:,-1].astype(np.int32)
    ply = PlyData([PlyElement.describe(vertex_all, 'vertex')], text=True)
    ply.write(outpath)
    
        
        
        
def parse_args():
    parser = argparse.ArgumentParser(description="download pointcloud data and convert to usable .ply format")
    parser.add_argument('--basedir', default="/datadrive/AHN3_pointcloud/POC_1",
                        help='path to the pointcloud dataset')
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    args = parse_args()

    bladindices = get_metadata()

    for split in ['train', 'test']:
        lasdir = os.path.join(args.basedir, 'las', split)
        lasfiles = glob.glob(lasdir + "/*.las")
        random.shuffle(lasfiles)
        lasfiles.remove("/datadrive/AHN3_pointcloud/POC_2/las/train/C_14CZ1.las")
        # lasfiles.remove("/datadrive/AHN3_pointcloud/POC_2/las/train/C_14CZ1.las")
        logging.info(f"lasfiles: {lasfiles}")

        for lasfile in lasfiles:
            key = os.path.basename(lasfile).split('.')[0]
            [metadict] = [elem for elem in bladindices['features'] if elem['properties']['bladnr'].upper() == key[2:]]

            las_to_ply(lasfile, metadict)