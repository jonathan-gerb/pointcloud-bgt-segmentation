import numpy as np
# import urllib3
import logging
import requests
import os
import glob
from laspy.file import File
import numpy as np
import random
from plyfile import PlyData, PlyElement
import struct
from multiprocessing import Pool
import math
import json

def get_download_keys():
    """Get lazfile data from georegister, returns list of lazfile names."""
    url = "http://geodata.nationaalgeoregister.nl/ahn3/wfs?request=getFeature&service=WFS&typeName=ahn3_bladindex&version=2.0.0&outputFormat=application/json"
    resp = requests.get(url=url)
    data = resp.json()
    return ["C_" + feat['properties']['bladnr'].upper() for feat in data['features']]


def download_laz_files(args):
    """Download all laz files."""
    keys = get_download_keys()
    random.shuffle(keys)

    for key in keys:
        lazdir = os.path.join(args.basedir, 'laz')
        download_laz(key, lazdir)
        

def download_laz(key, savedir):
    """
    Download pointcloud .LAZ file using key (bladindex).

    example:
    - link: 'https://download.pdok.nl/rws/ahn3/v1_0/laz/C_31AZ2.LAZ'
    - key: 'C_31AZ2'
    """
    url = f'https://download.pdok.nl/rws/ahn3/v1_0/laz/{key}.LAZ'
    fpath = os.path.join(savedir, f"{key}.LAZ"

    if os.path.exists(fpath):
        logging.info(f"file {fpath} already exists, skipping query")
        return

    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        with open(fpath, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192):
                # If you have chunk encoded response uncomment if
                # and set chunk_size parameter to None.
                # if chunk:
                f.write(chunk)


def parse_args():
    parser = argparse.ArgumentParser(description="download pointcloud data and convert to usable .ply format")
    parser.add_argument('--basedir', default="./pointclouds",
                        help='path to the pointcloud dataset')
    args = parser.parse_args()
        
if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    args = parse_args()
                         
    download_laz_files(args)
