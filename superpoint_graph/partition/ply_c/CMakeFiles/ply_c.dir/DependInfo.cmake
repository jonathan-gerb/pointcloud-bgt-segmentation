# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/jonathan/repositories/pointcloud-bgt-segmentation/superpoint_graph/partition/ply_c/ply_c.cpp" "/home/jonathan/repositories/pointcloud-bgt-segmentation/superpoint_graph/partition/ply_c/CMakeFiles/ply_c.dir/ply_c.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BOOST_ALL_NO_LIB"
  "BOOST_NUMPY_DYN_LINK"
  "BOOST_PYTHON_DYN_LINK"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/datadrive/anaconda3/envs/pointcloud/include"
  "/datadrive/anaconda3/envs/pointcloud/include/eigen3"
  "/datadrive/anaconda3/envs/pointcloud/include/python3.6m"
  "/datadrive/anaconda3/envs/pointcloud/lib/python3.6/site-packages/numpy/core/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
