CUDA_VISIBLE_DEVICES=0
cd superpoint_graph
while ! python learning/main.py --CUSTOM_SET_PATH /datadrive/AHN3_pointcloud/POC_1 --pc_attribs xyz --dataset custom_dataset --batch_size 20 --epochs 270 --nworkers 6 --lr 0.01 --pc_augm_mirror_prob 0.2 --lr_decay 0.7 --pc_augm_rot 1 --lr_steps=[200,230] --resume results/model.pth.tar
do
	sleep 1
	echo "Restarting program..."
done
