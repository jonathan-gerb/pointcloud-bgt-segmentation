import numpy as np
# import urllib3
import logging
import requests
import os
import glob
from laspy.file import File
import numpy as np
import random
from plyfile import PlyData, PlyElement
import struct
from multiprocessing import Pool
import math
import argparse
import json

def get_metadata():
    url = "http://geodata.nationaalgeoregister.nl/ahn3/wfs?request=getFeature&service=WFS&typeName=ahn3_bladindex&version=2.0.0&outputFormat=application/json"
    resp = requests.get(url=url)
    data = resp.json()
    return data


def convert_and_save_tile(topleft_x, topleft_y, pointcloud):
    """in put in RD meters"""
    logging.info(f"converting key: {topleft_x}, {topleft_y}")
    left_x = topleft_x  # convert to mm from meter
    top_y = topleft_y  # convert to mm from meter
    right_x = (topleft_x + 250)  # 1 km to the right
    bot_y = (topleft_y - 250)   # 1 km to the right
    # print(pointcloud.x.shape)
    # print(pointcloud.x[:5])
    # print(pointcloud.y[:5])

    logging.info(f"filtering points")
    print(f"left_x: {left_x}, right_x: {right_x}, bot_y: {bot_y}, top_y: {top_y}")
    # print(pointcloimport structud.x[pointcloud.x > left_x].shape)
    # print(pointcloud.x[(pointcloud.x > left_x) & (pointcloud.x < right_x)].shape)
    # print(pointcloud.x[(pointcloud.x > left_x) & (pointcloud.x < right_x) & (pointcloud.y > bot_y)].shape)
    # print(pointcloud.x[(pointcloud.x > left_x) & (pointcloud.x < right_x) & (pointcloud.y > bot_y) & (pointcloud.y < top_y)].shape)
    newx = pointcloud.x[(pointcloud.x > left_x) & (pointcloud.x < right_x) & (pointcloud.y > bot_y) & (pointcloud.y < top_y)]
    newy = pointcloud.y[(pointcloud.x > left_x) & (pointcloud.x < right_x) & (pointcloud.y > bot_y) & (pointcloud.y < top_y)]
    newz = pointcloud.z[(pointcloud.x > left_x) & (pointcloud.x < right_x) & (pointcloud.y > bot_y) & (pointcloud.y < top_y)]
    newscanangle = pointcloud.scan_angle_rank[(pointcloud.x > left_x) & (pointcloud.x < right_x) & (pointcloud.y > bot_y) & (pointcloud.y < top_y)]
    newintensity = pointcloud.intensity[(pointcloud.x > left_x) & (pointcloud.x < right_x) & (pointcloud.y > bot_y) & (pointcloud.y < top_y)]
    newflagbyte = pointcloud.flag_byte[(pointcloud.x > left_x) & (pointcloud.x < right_x) & (pointcloud.y > bot_y) & (pointcloud.y < top_y)]
    newclass = pointcloud.classification[(pointcloud.x > left_x) & (pointcloud.x < right_x) & (pointcloud.y > bot_y) & (pointcloud.y < top_y)]

    logging.info(f"moving pointcloud to 0,0 origin")
    if newx.shape[0] > 0:
        print(f"first point in cloud: ({newx[0]},{newy[0]})")
    else:
        print("no points in area")
        return
    # move x and y positions to zero and transform distances to meters
    newx = (newx - left_x) / 1000
    newy = (newy - bot_y) / 1000
    newz = newz / 1000

    # keys are kitty labels, values are ahn labels
#     conversion_dict = {
#         "1": 9,cloud
#         "2": 0,
#         "6": 3,bladnr
#         "9": 13,
#         "26": 3,
#     }

    # conversion dict for new classes
    conversion_dict = {
        "1": 1,
        "2": 2,
        "6": 3,
        "9": 4,
        "26": 5,
    }

    # convert classes in ahn data
    logging.info(f"replacing classes in AHN pointcloud")
    for i in range(len(newclass)):
        newclass[i] = conversion_dict[str(newclass[i])]

    # make the new dataset
    # out_arr = np.zeros((newy.shape[0], 7), dtype=np.float32)
    xyz_arr = np.zeros((newy.shape[0], 3), dtype=np.float32)
    # print(out_arr.shape)

    xyz_arr[:, 0] = newx
    xyz_arr[:, 1] = newy
    xyz_arr[:, 2] = newz

    # out_arr[:, 0] = newx
    # out_arr[:, 1] = newy
    # out_arr[:, 2] = newz
    # out_arr[:, 3] = newscanangle
    # out_arr[:, 4] = newintensity
    # out_arr[:, 5] = newflagbyte
    # out_arr[:, 6] = newclass

    # outpath = f"/datadrive/AHN3_pointcloud/source/ply/{str(int(left_x)) + '_' + str(int(top_y))}.ply"
    outpath = f"/datadrive/AHN3_pointcloud/POC_1/data/test/{str(int(left_x)) + '_' + str(int(top_y))}.ply"

    logging.info(f"saving to: {outpath}")
    logging.info(f"key (left_x, top_y)={int(left_x)}, {int(top_y)}")
    # np.savez_compressed(outpath, data=out_arr)
    # write_pointcloud(outpath, xyz_arr, newclass.astype(np.int32))
    write_pointcloud_plydata(outpath, xyz_arr, newclass)


def las_to_ply(lasfile, metadata_dict):
    
    key = metadata_dict['properties']['bladnr']

    # contents of metadata_dict: 
    # [[200000, 618750], [205000, 618750], [205000, 612500], [200000, 612500], [200000, 618750]] = metadata_dict['geometry']['coordinates'][0][0])
    # in meters
    # [topleft, topright, botleft, botright] = metadata_dict['geometry']['coordinates'][0][0]

    # works for almost all cases,
    topleft = metadata_dict['geometry']['coordinates'][0][0][0]
    topright = metadata_dict['geometry']['coordinates'][0][0][1]
    botleft = metadata_dict['geometry']['coordinates'][0][0][2]
    botright = metadata_dict['geometry']['coordinates'][0][0][3]
    botleft = [int(coord/10)*10 for coord in botleft]
    topright = [int(coord/10)*10 for coord in topright]
    topleft = [int(coord/10)*10 for coord in topleft]
    botright = [int(coord/10)*10 for coord in botright]
    
    # make areas of 250m2
    keys = []
    for x in range(topleft[0], topright[0], 250):
        for y in range(botright[1], topright[1], 250):
            y += 250
            key = (x, y)
            keys.append(key)
                
    logging.info(f"converting ahn bladindex key: {key}")

    pointcloud = File(f'/datadrive/AHN3_pointcloud/source/las/C_{key.upper()}.las', mode='r')
    # max_x = np.max(pointcloud.X)
    # min_x = np.min(pointcloud.X)
    # max_y = np.max(pointcloud.Y)
    # min_y = np.min(pointcloud.Y)

    # right_tile_x = int(round(max_x / 1000000))  # kilometer coordinates
    # left_tile_x = int(round(min_x / 1000000))  # kilometer coordinates
    # top_tile_y = int(round(max_y / 1000000))  # kilometer coordinates
    # bot_tile_y = int(round(min_y / 1000000))  # kilometer coordinates

    keys = []
    # print(f"left of tile x: {left_tile_x}, right of tile x: {right_tile_x}")
    # print(f"top of tile y: {top_tile_y}, bot of tile y: {bot_tile_y}")
    print(topleft, topright, botleft, botright)
    print(f"{topleft[0]} to {topright[0]}")
    print(f"{botright[1]} to {topright[1]}")
    for x in range(topleft[0], topright[0], 250):
        for y in range(botright[1], topright[1], 250):
            # move botleft to topleft coordinate
            y += 250
            # print(f"key: {x}_{y}")
            key = (x, y)
            keys.append(key)
    for key in keys:
        # outpath = f"/datadrive/AHN3_pointcloud/source/ply/{str(int(key[0])) + '_' + str(int(key[1]))}.ply"
        
        # instead of writing to a single source location and then later distributing the ply files across the test and train folders
        # it is easier to just write all of them to the train folder so the script can run while i'm away.
        outpath = f"/datadrive/AHN3_pointcloud/POC_1/data/test/{str(int(key[0])) + '_' + str(int(key[1]))}.ply"
        
        if not os.path.exists(outpath):
            convert_and_save_tile(key[0], key[1], pointcloud)
        else:
            print(f"skipping key: {key[0]}_{key[1]}")
        


def write_pointcloud_plydata(filename, xyz, classes):
    prop = [('x', 'f4'), ('y', 'f4'), ('z', 'f4'),  ('label', 'i4')]
    vertex_all = np.empty(len(xyz), dtype=prop)
    for i in range(0, 3):
        vertex_all[prop[i][0]] = xyz[:, i]

    vertex_all[prop[3][0]] = classes.astype(np.int32)
    ply = PlyData([PlyElement.describe(vertex_all, 'vertex')], text=True)
    ply.write(filename)


def write_pointcloud(filename, xyz_points, classes, rgb_points=None):

    """ creates a .pkl file of the point clouds generated

    """

    assert xyz_points.shape[1] == 3, 'Input XYZ points should be Nx3 float array'
    if rgb_points is None:
        rgb_points = np.ones(xyz_points.shape).astype(np.uint8)*255
    assert xyz_points.shape == rgb_points.shape,'Input RGB colors should be Nx3 float array and have same size as input XYZ points'

    # Write header of .ply file
    fid = open(filename, 'wb')
    fid.write(bytes('ply\n', 'utf-8'))
    fid.write(bytes('format binary_little_endian 1.0\n', 'utf-8'))
    fid.write(bytes('element vertex %d\n'%xyz_points.shape[0], 'utf-8'))
    fid.write(bytes('property float x\n', 'utf-8'))
    fid.write(bytes('property float y\n', 'utf-8'))
    fid.write(bytes('property float z\n', 'utf-8'))
    fid.write(bytes('property uchar red\n', 'utf-8'))
    fid.write(bytes('property uchar green\n', 'utf-8'))
    fid.write(bytes('property uchar blue\n', 'utf-8'))
    fid.write(bytes('property long label\n', 'utf-8'))
    fid.write(bytes('end_header\n', 'utf-8'))

    # Write 3D points to .ply file
    for i in range(xyz_points.shape[0]):
        fid.write(bytearray(struct.pack("fffcccl", xyz_points[i, 0], xyz_points[i, 1], xyz_points[i, 2],
                                        rgb_points[i, 0].tostring(), rgb_points[i, 1].tostring(),
                                        rgb_points[i, 2].tostring(), classes[i])))
    fid.close()


def parse_args():
    parser = argparse.ArgumentParser(description="download pointcloud data and convert to usable .ply format")
    parser.add_argument('--basedir', default="/datadrive/AHN3_pointcloud/POC_1",
                        help='path to the pointcloud dataset')
    args = parser.parse_args()
    return args
        
if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    args = parse_args()
    
    # run the unzip command using lastools before running this script
    # to unzip all the laz files. manually move these files to the correct folder
    # ie. move all the .las files to the las/ folder.
    # find . -name '*.LAZ' | xargs --max-procs=5 --max-args=1 laszip -i                   
    

    # split up the uncompressed las files to smaller ply files of 250m^2
    bladindices = get_metadata()

#     random.shuffle(bladindices['features'])
    for split in ['train', 'test']:
        lasdir = os.path.join(args.basedir, 'las', split)
        print(f"lasdir: {lasdir}")
        print(lasdir + "/*.las")
        lasfiles = glob.glob(lasdir + "/*.las")

        print(lasfiles)
        for lasfile in lasfiles:
            key = os.path.basename(lasfile).split('.')[0]
            [metadict] = [elem for elem in bladindices['features'] if elem['properties']['bladnr'].upper() == key[2:]]
            
            las_to_ply(lasfile, metadict)