#!/bin/bash 
for i in 104750_526500 135000_420250 135000_422000 135250_420500 136500_424000 137750_423000 190000_469000 190500_471500 243750_600000 135000_420000 135000_421250 135000_422250 135500_419750 136500_425000 139500_424500 190250_471000 240750_596500
do
   python superpoint_graph/partition/visualize.py --dataset custom_dataset --ROOT_PATH /datadrive/AHN3_pointcloud/POC_1 --res_file ~/repositories/pointcloud-bgt-segmentation/superpoint_graph/results/predictions_test --output_type grspf --file_path test/$i
done

# python superpoint_graph/partition/visualize.py --dataset custom_dataset --ROOT_PATH /datadrive/AHN3_pointcloud/POC_1 --res_file ~/repositories/pointcloud-bgt-segmentation/superpoint_graph/results/predictions_test --output_type grspf --file_path test/104750_526500


# 104750_526500.h5
# 135000_421250.h5
# 135250_420500.h5
# 190000_469000.h5                                                                      
# 135000_420000.h5  
# 135000_422000.h5
# 135500_419750.h5
# 240750_596500.h5                                                                      
# 135000_420250.h5  
# 135000_422250.h5
# 136500_424000.h5
# for i in 135000_421250 135250_420500 190000_469000 135000_420000 135000_422000 135500_419750 240750_596500 135000_420250 240750_596500 135000_420250 135000_422250 136500_424000 104750_526500

# for i in 135000_422000 135000_420250 135250_420500 135500_419750 137750_4230000 139500_424500 190250_471000 190500_471500 243750_600000